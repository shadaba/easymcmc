#Author: Shadab Alam, Nov 2020
#This package can do conditional string interpolations

from string import Template
import configobj

import re

class Cstring_interp():
    def __init__ (self,vdic,maxdeep=100):
        #self.string=strin #input string which needs to be replaced
        self.vdic=vdic #a dictionary of values to put condition on and replace
        self.open='^'  
        self.close=';'
        self.condition=':'
        #maximum rescursive call after which the danger of infinite loop so quit
        self.maxdeep=100

    def split_open_recursive(self,strin,deep=0):
        if(deep>=self.maxdeep):
            raise ValueError('Exceeds max recursion limit (maxdeep=%d)'%(self.maxdeep))

        #find the first open and first close
        ind_open=strin.find(self.open)
        ind_close=strin.find(self.close)

        if(deep==0):
            if(ind_open==-1 and ind_close==-1):
                str_out=strin
            elif((ind_open==-1 and ind_close>=0) or (ind_open>=0 and ind_close==-1) or ind_open>ind_close):
                raise ValueError('not well matched string: %s'%(strin))
            else:
                str_mid=self.split_open_recursive(strin[ind_open+1:ind_close],deep=deep+1)
                str_out=strin[:ind_open]+str_mid+strin[ind_close+1:]
                #recursive call fo second block
                str_out=self.split_open_recursive(str_out,deep=0)
        elif(deep>0):
            if(ind_open==-1 and ind_close==-1):
                str_out=self.replace(strin)
            elif(ind_open>=0 and ind_close==-1):
                str_mid=self.replace(strin[ind_open+1:])
                str_out=strin[:ind_open]+str_mid
            elif((ind_open==-1 and ind_close>=0) or ind_close<ind_open):
                raise ValueError('not well matched string: %s'%(strin))
            else:
                str_mid=self.split_open_recursive(strin[ind_open+1:ind_close],deep=deep+1)
                str_out=strin[:ind_open]+str_mid+strin[ind_close+1:]


        return str_out

    def replace(self,strin):
        '''make the replacement in the string'''
        
        args=self.vdic
        if(self.condition in strin):
            tspl=strin.split(self.condition)
            if(len(tspl)>2):
                raise ValueError('more than one condition (%s) in one block <>  for string %s'%(self.condition,strin))
            
            if(eval(tspl[0])):
                str_out=self.replace(tspl[1])
            else:
                str_out=''
        elif('args[' in strin):
            str_out=eval(strin)
            assert(str_out!=None),"a input variable is set to None: %s"%(strin)
        else:
            str_out=strin

        return str_out




def interp_dic(dicin,child_dic={},verbose=0):
    '''scans the dicin and interpolate for strings with conditions input
    TODO: This can only interpolate the dictionaries, for config objects the sections have complex type and the code ignores them for now
    Need to figiure out how to convert configobj.section to dictionary and update values'''


    csi=Cstring_interp(dicin)

    #if we want to scan a nested dictionary the child_dic is useful
    if(child_dic=={}):
        child_dic=dicin

    for tt,tkey in enumerate(child_dic.keys()):
        if(type(child_dic[tkey])==str):
            tbefore=child_dic[tkey]
            child_dic[tkey]=csi.split_open_recursive(child_dic[tkey],deep=0)
            if(tbefore!=child_dic[tkey] and verbose>1):
                print('\n *** Applied Conditional String Interpolation on : %s'%tkey)
                print('  %s\n\t--->'%tbefore)
                print('  %s\n'%child_dic[tkey])
        elif(type(child_dic[tkey])==dict):
            child_dic[tkey]=interp_dic(dicin,child_dic=child_dic[tkey],verbose=verbose)
        #elif(type(child_dic[tkey]) not in [float, bool ,int, list ]):
        #    child_dic[tkey]=interp_dic(dicin,child_dic=child_dic[tkey],verbose=verbose)

    return child_dic


if __name__=='__main__':
    test_str="shadab^args['pip']:xxx;^args['iip']:Alam;"
    test_dic={'pip':True,'iip':True}
    csi=Cstring_interp(test_dic)
    str_out=csi.split_open_recursive(test_str,deep=0)

    print('input : ',test_str)
    print('result: ',str_out)
