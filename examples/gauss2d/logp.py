import numpy as np


def Load_data(cfg,like_name=''):
    '''Loads and return any important data contained in a dictionary which is passed to lnlike function'''
    data={}
    data['a']=1.0
    data['b']=0.5
    data['c']=1.0
    data['cov']=np.array([[0.5,0.0,0],
                          [0.0,0.5,0.2],
                          [0,0.2,1.0]])

    data['icov']=np.linalg.inv(data['cov'])

    return data

def lnlike(pardic,data_dic,cfg):
    '''returns lnlike'''

    diff=np.array([pardic['a']-data_dic['a'],
                  pardic['b']-data_dic['b'],
                  pardic['c']-data_dic['c']])

    chi2=np.dot(diff,data_dic['icov'])
    chi2=np.dot(chi2,diff.T)

    log_like=-0.5*chi2
    #like_this=np.exp(-0.5*chi2)

    return log_like, [False]

