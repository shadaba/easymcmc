#Author: Shadab Alam, 27 May 2021

import sys
import os

import numpy as np

#model for GP
sys.path=['/global/homes/s/shadaba/Projects/HODFitter']+sys.path
import GP_HOD_model as GPHOD



def load_wedge_2d(fname):
    '''loads the wedge 2d correlation function file'''
    tlines=open(fname,'r').readlines()
    tt=0
    while(tlines[tt][0]=='#'):
        tline=tlines[tt]
        if('#log10r:' in tline):
            logr=np.array(tline[8:].split(),dtype=float)
            rr=np.power(10,logr)
            nrbin=rr.size
        elif('#logr:' in tline):
            logr=np.array(tline[6:].split(),dtype=float)
            rr=np.power(10,logr)
            nrbin=rr.size
        elif('#mu:' in tline):
            mubin=np.array(tline[4:].split(),dtype=float)
            nmubin=mubin.size

        tt=tt+1
        if(tt>20):
            break
    rr_arr=rr.copy()
    for mm in range(1,nmubin):
        rr_arr=np.append(rr_arr,rr)

    wedge2d=np.loadtxt(fname)
    #the order is different mu first and then r so this needs to be reorganized for the model
    tdat_new=np.zeros(wedge2d.shape)
    for jj in range(0,wedge2d.shape[1]):
        this_col=wedge2d[:,jj].reshape(nrbin,nmubin)
        tdat_new[:,jj]=np.append(np.append(this_col[:,0],this_col[:,1])
                ,this_col[:,2])

    wpxiall=np.column_stack([rr_arr,tdat_new])
    #print(wpxiall[:,:3])
    return wpxiall, nrbin,nmubin


def Load_data(cfg,like_name=''):
    '''Loads and return any important data contained in a dictionary which is passed to lnlike function'''
    data={}

    data['wedge'],nrbin,nmubin=load_wedge_2d(cfg['wedge_file'])
    
    njn=data['wedge'].shape[1]-4

    data['xval']=data['wedge'][:,0]
    data['indsel']=np.ones(data['xval'].size,dtype=bool)
    
    if('wedge_use' in cfg.keys()):
        isel=data['indsel']
        for ww in [0,1,2]:
            wmin=ww*nrbin;wmax=nrbin*(ww+1)
            if(ww in cfg['wedge_use']):
                isel[wmin:wmax]=isel[wmin:wmax]*True
            else:
                isel[wmin:wmax]=isel[wmin:wmax]*False

        data['indsel']=isel
        print('using wedges:',cfg['wedge_use'])

    if('wedge_slim' in cfg.keys()):
        indsel=data['xval']>cfg['wedge_slim'][0]
        indsel=indsel*(data['xval']<cfg['wedge_slim'][1])
        data['indsel']=data['indsel']*indsel
        print('using scale selections:',cfg['wedge_slim'])
        print('xval post selection:',data['xval'][data['indsel']])
        print('xval mask:',data['indsel'])



    data['mu']=data['wedge'][data['indsel'],3]
    data['cov']=np.cov(data['wedge'][data['indsel'],4:])*njn
    data['icov']=np.linalg.inv(data['cov'])

    #setup GP
    #load the model
    data['GPmodel']=GPHOD.load_models(cfg['snap'],sim=cfg['sim'],model_dir=cfg['model_dir'])

    #set the cv class Also trains the model and write the results
    data['GP']=GPHOD.cross_valid(data['GPmodel'],outtag=cfg['outtag'],ibeg=58,iend=None)

    #parameter order
    pmap=data['GPmodel']['meta']['pmap']
    porder=[None]*len(pmap.keys())
    for pp,par in enumerate(pmap.keys()):
        porder[pmap[par]]=par

    data['GPporder']=porder


   

    #debug
    if(True): #just to make the first call to models
        import mcmc_utility as mcutil
        pos0=mcutil.initialize_par(cfg,data['GPmodel']['theta'].shape[1],2)#nwalkers)
        LD=cfg['LD'].copy()
        LD=mcutil.map_param(pos0[0],cfg,LD)
        model,blob=evaluate_model(LD,data,cfg)
        #print(model.shape,blob.shape)

    return data

def evaluate_model(pardic,data_dic,cfg):
    '''evaluates the model using GPHOD'''

    X=[]
    for pp,par in enumerate(data_dic['GPporder']):
       X.append(pardic[par])
    X=np.array(X)
    
    model=data_dic['GP'].predict_model(X,stat='wedge',usecache=True,
            kmin=None,kmax=None,rmin=None,rmax=None)

    blob=data_dic['GP'].predict_model(X,stat='Ngalbias',usecache=True,
            kmin=None,kmax=None,rmin=None,rmax=None)

    return model.flatten(),blob.flatten()

def lnlike(pardic,data_dic,cfg):
    '''returns lnlike'''

    model,blob=evaluate_model(pardic,data_dic,cfg)
    diff=data_dic['mu']-model[data_dic['indsel']]

    chi2=np.dot(diff,data_dic['icov'])
    chi2=np.dot(chi2,diff.T)

    log_like=-0.5*chi2
    #like_this=np.exp(-0.5*chi2)
    if(cfg['use_bias']):
        #also using linear bias in the likelihood
        chi2_blin=np.power((blob[-1]-cfg['LinearBias']['mean'])/cfg['LinearBias']['sigma'],2)
        log_blin=-0.5*chi2_blin
        log_like=log_like+log_blin
        #print(log_like,log_blin,blob,cfg['LinearBias']['mean'],cfg['LinearBias']['sigma'])
    
    if(cfg['Sampler']['name']=='zeus'):
        return log_like,False #, blob
    else:
        return log_like, blob

