import os
import numpy as np
from scipy.interpolate import CubicSpline
from scipy.integrate import quad_vec, quad
from functools import partial



def load_wp(fname):
    '''loads the projected correlation function (wp) file'''

    if(not os.path.isfile(fname)):
        print('wp file not found: %s'%fname)
        sys.exit(-1)

    wp=np.loadtxt(fname)
    return wp



def Load_data(cfg,like_name=''):
    '''Loads and return any important data contained in a dictionary which is passed to lnlike function'''
    data={}

    data['wp']=load_wp(cfg['wp_file'])

    njn=data['wp'].shape[1]-4
    
    #get the x-axis
    data['xval']=data['wp'][:,0]
    #apply selection
    isel=data['xval']>cfg['wp_rperlim'][0]
    isel=isel*(data['xval']<cfg['wp_rperlim'][1])
    data['indsel']=isel
    print('Selected rp using wp_rperlim: ',cfg['wp_rperlim'])


    #apply mask
    imask_2d=np.isnan(data['wp'])+np.isinf(data['wp'])
    imask=imask_2d.sum(axis=1)!=0
    nsel=data['indsel'].sum()
    data['indsel']=data['indsel']* ~ imask
    print('Applied mask for nan and inf: %d/%d'%(data['indsel'].sum(),nsel))  


    print('nan:',np.isnan(data['wp'][data['indsel'],4:]).sum())
    print('inf:',np.isinf(data['wp'][data['indsel'],4:]).sum())
    data['mu']=data['wp'][data['indsel'],3]
    data['cov']=np.cov(data['wp'][data['indsel'],4:])*njn
    data['icov']=np.linalg.inv(data['cov'])


    #debug
    if(False): #just to make the first call to models
        import mcmc_utility as mcutil
        pos0=mcutil.initialize_par(cfg,cfg['Sampler']['ndim'],2)#nwalkers)
        LD=cfg['LD'].copy()
        LD=mcutil.map_param(pos0[0],cfg,LD)
        model,blob=evaluate_model(LD,data,cfg)
        #print(model.shape,blob.shape)

    return data


# Define the DK-14 profile equations
def dk14_numerical(r_s, r_t, rho_s, rho_o, alpha, beta, gamma, s_e, R):
    # Implement the DK-14 profile equations here
    r_out = 1.5  # h^-1 Mpc
    rho_in = rho_s * np.exp(-2/alpha * ((R/r_s)**alpha - 1))
    rho_outer = rho_o * (R / r_out)**(-s_e)
    f_trans = (1 + (R / r_t) ** beta) ** (-gamma / beta)
    rho_R = (rho_in * f_trans) + rho_outer
    return rho_R

def sigma_dk14(pardic,rper_arr,rpi_max=30):
    dk14_partial = partial(
        dk14_numerical,
        r_s=pardic['r_s'], r_t=pardic['r_t'], rho_s=pardic['rho_s'], rho_o=pardic['rho_o'],
        alpha=pardic['alpha'], beta=pardic['beta'], gamma=pardic['gamma'], s_e=pardic['s_e'],
        )
    def dk14_integrand(rpi):
        return dk14_partial(R=np.sqrt(rper_arr**2 + rpi**2))

    sigma_r_dk14 = 2 * quad_vec(dk14_integrand, 0, rpi_max)[0]
    return sigma_r_dk14


# Define the initialization function
def initial_args_for_dk14(r_data):
    args_for_sigma = [
        np.exp(1.1),
        np.exp(0.349),
        np.exp(-0.32),
        np.exp(-0.082),
        np.exp(-0.95),
        np.exp(0.762),
        np.exp(0.66),
        1.601,
        ]
    return tuple(args_for_sigma + [r_data])



def evaluate_model(pardic,data_dic,cfg):
    '''evaluates the model using GPHOD'''

    # Calculate projected density using the DK-14 model
    projected_density = sigma_dk14(pardic,data_dic['xval'],rpi_max=cfg['rpi_max'])
    blob=[False]

    return projected_density ,blob

#This function is necessary to evaluate the best fit model
def best_model(pardic,data_dic,cfg):
    '''evaluates the model using GPHOD'''

    if('xval_best' not in data_dic.keys()):
        dlogR=0.01
        R_min=data_dic['xval'].min()
        R_max=data_dic['xval'].max()
        nR=int((R_max-R_min)/dlogR)
        logR_sampling=np.linspace(np.log10(R_min),np.log10(R_max),nR)
        data_dic['xval_best']=np.power(10,logR_sampling)

    # Calculate projected density using the DK-14 model
    projected_density = sigma_dk14(pardic,data_dic['xval_best'],rpi_max=cfg['rpi_max'])
    blob=[False]

    return data_dic['xval_best'],projected_density



def lnlike(pardic,data_dic,cfg):
    '''returns lnlike'''

    model,blob=evaluate_model(pardic,data_dic,cfg)
    diff=data_dic['mu']-model[data_dic['indsel']]

    chi2=np.dot(diff,data_dic['icov'])
    chi2=np.dot(chi2,diff.T)

    log_like=-0.5*chi2
    #like_this=np.exp(-0.5*chi2)
    #if(cfg['use_bias']):
    #    #also using linear bias in the likelihood
    #    chi2_blin=np.power((blob[-1]-cfg['LinearBias']['mean'])/cfg['LinearBias']['sigma'],2)
    #    log_blin=-0.5*chi2_blin
    #    log_like=log_like+log_blin
    #    #print(log_like,log_blin,blob,cfg['LinearBias']['mean'],cfg['LinearBias']['sigma'])

    if(cfg['Sampler']['name']=='zeus'):
        return log_like,False #, blob
    else:
        return log_like, blob



