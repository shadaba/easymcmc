import pickle
import numpy as np
import pylab as pl
import time

from datetime import date

import os

import pyccl as ccl
import numpy as np

import pickle
import scipy

#update syspath by removing python2.7 things

import sys
if(True):
    npath=[]
    for pp in sys.path:
        if('python2.7/' in pp):
            continue
        npath.append(pp)
    sys.path=npath

    sys.path=['/global/homes/s/shadaba/Projects/shadab_pythonlib/']+sys.path


import dump_dictionary as dumpdic

sys.path=['/Users/shock/Documents/Projects/OmegaTheta/']+sys.path
import Angular_corr_model as Angcorr

def get_wtheta_file(tracer='LRG',sky='NDECALS',selection='full',wtag='w1',mask='',
                    wth_dir='/global/project/projectdirs/desi/users/shadaba/SV3Targets/XI/Wtheta_29Apr2021v1/'):
    '''returns the wtheta file'''

    outroot='%s%s_%s_%s_%s%s'%(wth_dir,tracer,sky,selection,wtag,mask)

    outfile='%s-wtheta_LS-logangular-NJN-%d.txt'%(outroot,get_njn(sky))

    return outfile

def get_njn(sky):
    #njn_dic={'NDECALS':440,'SDECALS':600,'NBMZLS':385,'DES':23*15,'SDECALS_noDES':19*14}
    njn_dic={'NDECALS':34*10,'SDECALS':25*20,'NBMZLS':27*11,'DES':20*14,'SDECALS_noDES':24*19,
            'DES_noLMC':24*11}

    return njn_dic[sky]


def get_nz(zbin=-1,tracer='LRG',outtag='29Apr2021v1',return_nbin=False,
           model_dir='/global/u1/s/shadaba/Projects/SV3/NNresults/'):
    '''load the nz for different subsamples
    zbins=-1 full sample otherwise photozbin'''


    outfile='%s%s%s.pkl_histdic'%(model_dir,tracer,outtag)

    zhist_dic=dumpdic.dump_load_dic_fits(outfile,verbose=0,tabin='',nodic=0)

    nz=zhist_dic['zbin%d'%(zbin)]

    if(return_nbin):
        bin_key=list(zhist_dic.keys())
        if('nzbin' in bin_key):
            nbin=zhist_dic['nzbin']
        else:
            nbin=0
            for tt,tkey in enumerate(bin_key):
                if('zbin'==tkey[:4] and tkey!='zbin-1'):
                    nbin=nbin+1
        return nz,nbin

    return nz


def get_omtheta_model(theta_deg,nz,blinz,omth=None,cosmotag=None,ellmax=2000):

    if(omth is None):
        assert(cosmotag is not None)

        omth={'cosmotag':cosmotag,
             'cosmo':get_cosmology(cosmotag)}

        Omega_c=omth['cosmo']['omc'];
        Omega_b=omth['cosmo']['omb'];
        h=omth['cosmo']['h'];
        sigma8=omth['cosmo']['sigma8'];
        n_s=omth['cosmo']['ns']

        omth['ccl_cosmo'] = ccl.Cosmology(Omega_c= Omega_c, Omega_b=Omega_b, h=h, sigma8=sigma8, n_s=n_s,
                transfer_function='bbks')

        omth['ell'] = np.arange(2, ellmax)

    clu1 = ccl.NumberCountsTracer(omth['ccl_cosmo'], has_rsd=False, dndz=(nz[:,0],nz[:,1]), bias=(blinz[:,0],blinz[:,1]))
    cls_clu = ccl.angular_cl(omth['ccl_cosmo'], clu1, clu1, omth['ell']) #Clustering

    #xi_clu = ccl.correlation(omth['ccl_cosmo'], omth['ell'], cls_clu, theta_deg, type='NN', method='FFTLog')
    xi_clu = ccl.correlation(omth['ccl_cosmo'], omth['ell'], cls_clu, theta_deg, corr_type='GG', method='FFTLog')

    return xi_clu, omth


# Some useful functions
def get_cosmology(cosmotag):
    if(cosmotag=='planck15'):
        #check section 2.1 of : https://arxiv.org/pdf/1502.01589.pdf
        cosmoin={'omk': 0, 'H0': 67.73, 'mnu': 0,
              'As': 2.19e-09, 'omch2': 0.1197, 'ns': 0.9655, 'ombh2': 0.02222,
                'sigma8':0.83}
    elif(cosmotag=='qpm'):
        #check table 2 of : https://arxiv.org/pdf/1606.00439.pdf
        cosmoin={'omk': 0, 'H0': 70.0, 'mnu': 0,
              'As': 2.19e-09, 'omch2': 0.11172, 'ns': 0.9655, 'ombh2': 0.0225}

    cosmoin['h2']=np.power(cosmoin['H0']/100.0,2)
    cosmoin['h']=cosmoin['H0']/100.0
    cosmoin['om0']=(cosmoin['omch2']+cosmoin['ombh2'])/cosmoin['h2']

    cosmoin['omc']=cosmoin['omch2']/cosmoin['h2']
    cosmoin['omb']=cosmoin['ombh2']/cosmoin['h2']

    return cosmoin


def get_bias_stat(obs,model,icov,bguess,bsigma=0.5):

    barr=np.linspace(bguess-5*bsigma,bguess+5*bsigma,100)
    chi2_arr=np.zeros(barr.size)
    dof=obs.size-1

    for ii in range(0,barr.size):
        diff=obs-(np.power(barr[ii],2)*model)
        chi2_arr[ii]=np.dot(diff,np.dot(icov,diff))


    ind_min=np.argmin(chi2_arr)
    b_best=barr[ind_min]
    chi_best=chi2_arr[ind_min]


    plike=np.exp(-chi2_arr/2)
    plike=plike/plike.sum()
    clike=plike.cumsum()

    val_lim_dic={'mid':0.5,'psigma':0.5+0.32,'nsigma':0.5-0.32}
    bfit_dic={}
    for tt,tkey in enumerate(val_lim_dic.keys()):
        tv=val_lim_dic[tkey]
        bfit_dic[tkey]=barr[np.argmin(np.abs(clike-tv))]

    bfit_dic['sigma']=0.5*(bfit_dic['psigma']-bfit_dic['nsigma'])
    bfit_dic['bfit']=b_best
    bfit_dic['bchi']=chi_best
    bfit_dic['dof']=model.size-1

    return bfit_dic

def load_data(cfg,tracer='LRG',sky='NDECALS',wtag='EdWsysv0',outtag='29Apr2021v1',fit_theta_lim_deg=[0.08,0.6]):
    '''load the wtheta measurements'''

    #get the number of bins
    nb,nzbin=get_nz(zbin=-1,tracer=tracer,outtag=outtag,return_nbin=True,model_dir=cfg['NNmodel_dir'])

    if('use_zbin' not in cfg.keys()):
        cfg['use_zbin']=np.arange(0,nzbin)
    elif(isinstance(cfg['use_zbin'],list)):
        cfg['use_zbin']=np.array(cfg['use_zbin'],dtype=int)
    print('nzbin:' ,nzbin)
    assert(cfg['nzbin']==cfg['use_zbin'].size)

    wth_dic={}
    #determine the zmaxlike for each bin
    wth_dic['zmax_pz']=np.zeros(cfg['nzbin'])
    setup=True
    tmsg='*** Following redshift bins are uses ***'
    czbin=0
    for ii in range(0,nzbin):
        if(ii not in cfg['use_zbin']):
            continue

        #selection='photo_zbin%d'%ii
        selection='tomo_v0_%d'%ii

        #The measurement of wtheta
        wth_file=get_wtheta_file(tracer=tracer,sky=sky,selection=selection,
                wtag=wtag,mask='',wth_dir=cfg['data_dir'])
        wth_data=np.loadtxt(wth_file)
        #load the nz
        nz_this=get_nz(zbin=ii,tracer=tracer,outtag=outtag,
                return_nbin=False,model_dir=cfg['NNmodel_dir'])
        
        ind_zmax=np.argmax(nz_this[:,1])
        wth_dic['zmax_pz'][czbin]=nz_this[ind_zmax,0]
        tmsg='%s\n bin=%d , zmax_pz=%5.3f '%(tmsg,ii,wth_dic['zmax_pz'][czbin])
        czbin=czbin+1

        wth_dic[selection]=wth_data
        if(setup):
            setup=False
            njn=wth_data.shape[1]-4
            #convert the theta in degree
            theta_deg =  wth_data[:,0]*180.0/np.pi
            indsel=(theta_deg>fit_theta_lim_deg[0])*(theta_deg<=fit_theta_lim_deg[1])
            obs_mat=wth_data[indsel,:]
            nz_mat=nz_this[:,:2]
        else:
            obs_mat=np.row_stack([obs_mat,wth_data[indsel,:]])
            nz_mat=np.column_stack([nz_mat,nz_this[:,1]])
    
    print(tmsg)

    #transfer these to dictionary
    wth_dic['njn']=njn
    wth_dic['theta_deg']=theta_deg[indsel]
    wth_dic['theta_rad']=wth_dic['theta_deg']*np.pi/180.0
    wth_dic['wtheta_mu']=obs_mat[:,3]
    wth_dic['cov']=np.cov(obs_mat[:,4:])*njn
    wth_dic['icov']=np.linalg.inv(wth_dic['cov'])
    wth_dic['nz_mat']=nz_mat
    wth_dic['omth']=None



    if(True):
        for tt,tkey in enumerate(wth_dic.keys()):
            if(isinstance(wth_dic[tkey],np.ndarray)):
               print(tt,tkey,wth_dic[tkey].shape)


    #setup the theory: This is slow as it precalculates theory basics
    wth_dic['Theory']=Angcorr.OmegaTheta(wth_dic['theta_rad'],cosmo=cfg['Theory']['cosmotag'],zpk=None,
            zmin=cfg['Theory']['zmin'],zmax=cfg['Theory']['zmax'],nzbin=cfg['Theory']['nzbin'],nzlos=500,nonlin=cfg['Theory']['nonlin'])


    return wth_dic

def Load_data(cfg,like_name=''):
    data_dic=load_data(cfg,tracer=cfg['tracer'],sky=cfg['sky'],wtag=cfg['wsys'],outtag=cfg['NNtomoversion'],fit_theta_lim_deg=cfg['fit_theta_lim'])
    return data_dic

def bias_model(zarr,pardic,cfg,data_dic,bzmodel='quadratic',zbin=None):
    '''bias evolution bzmodel can be anything sup[ported by scipy interp1d
    bzmodle: linear, slinear, quadratic, cubic'''
    
    if(bzmodel=='binned'):
        bz=np.zeros(zarr.size)+pardic['bz_%d'%zbin]
    else:
        bz_bin=np.zeros(cfg['nzbin'])
        for ii in range(0,cfg['nzbin']):
            tpar='bz_%d'%(ii)
            bz_bin[ii]=pardic[tpar]


        fint= scipy.interpolate.interp1d(data_dic['zmax_pz'] ,bz_bin,kind=bzmodel,fill_value="extrapolate")

        bz=fint(zarr)

    return np.column_stack([zarr,bz])

def wtheta_model(pardic,data_dic,cfg):

    nz_mat=data_dic['nz_mat']
    if(cfg['biasmodel']!='binned'):
        blinz=bias_model(nz_mat[:,0],pardic,cfg,data_dic,bzmodel=cfg['biasmodel'])

    #get the bias
    for ii in range(1,nz_mat.shape[1]): 
        #xi_clu, data_dic['omth']=get_omtheta_model(data_dic['theta_deg'],np.column_stack([nz_mat[:,0],nz_mat[:,ii]]),
        #                            blinz,omth=data_dic['omth'],cosmotag=cfg['cosmotag'],ellmax=cfg['ellmax'])
        if(cfg['biasmodel']=='binned'):
            blinz=bias_model(nz_mat[:,0],pardic,cfg,data_dic,bzmodel=cfg['biasmodel'],zbin=ii-1)

        xi_clu=data_dic['Theory'](np.column_stack([nz_mat[:,0],nz_mat[:,ii]]),blinz,validate_input=False)

        if(ii==1):
            model_mat=xi_clu[:,1]
        else:
            model_mat=np.append(model_mat,xi_clu[:,1])

    return model_mat

def lnlike(pardic,data_dic,cfg):
    '''returns lnlike'''

    model_mat=wtheta_model(pardic,data_dic,cfg)
    diff=data_dic['wtheta_mu']-model_mat

    chi2=np.dot(diff,data_dic['icov'])
    chi2=np.dot(chi2,diff.T)

    log_like=-0.5*chi2

    return log_like, False
