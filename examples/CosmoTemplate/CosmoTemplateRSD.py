import numpy as np
import pickle

import sys
sys.path.insert(0,'/Users/shock/Documents/Projects/EASYmcmc/examples/CosmoTemplate/')
import mycosmo as mcosm

def unit_pars():
    unit_dic={'z':0.9873,
    'ombh2' :0.02230,
    'omch2':0.1188,
    'omnuh2':0.00064,
    'omk': 0,
    'hubble': 67.74,
    's8z0':0.8147,
    'b1':None,
    'As':2.142e-9,
    }

    unit_dic['1e9As']=1e9*unit_dic['As']

    unit_dic['h']=unit_dic['hubble']/100.0
    unit_dic['H0']=unit_dic['hubble']
    unit_dic['omm']=(unit_dic['ombh2']+unit_dic['omch2'])/np.power(unit_dic['h'],2)
    unit_dic['oml']=1.0- unit_dic['omm']- unit_dic['omk']
    unit_dic['omb']=unit_dic['ombh2']/np.power(unit_dic['h'],2)

    unit_dic['Daz']=mcosm.D_A(unit_dic['z'], unit_dic['H0'], unit_dic['omm'], unit_dic['oml'])
    unit_dic['Hz']=mcosm.H_z(unit_dic['z'], unit_dic['H0'], unit_dic['omm'], unit_dic['oml'])
    unit_dic['s8z']=mcosm.sigma8z(unit_dic['z'], unit_dic['H0'], unit_dic['omm'], unit_dic['oml'])
    unit_dic['s8z']=unit_dic['s8z']*unit_dic['s8z0']

    unit_dic['OMz']=mcosm.omMz(unit_dic['z'], unit_dic['H0'], unit_dic['omm'], unit_dic['oml'])
    unit_dic['f']=np.power(unit_dic['OMz'],0.55)

    unit_dic['fs8']= unit_dic['f']* unit_dic['s8z']

    unit_dic['rs']= mcosm.r_s(unit_dic['omm'],unit_dic['omb'], unit_dic['H0'])

    #'Ase9', 'h','OmegaCDM','OmegaM','b1s8'
    unit_dic['Ase9']=unit_dic['1e9As']
    unit_dic['OmegaCDM']=unit_dic['omm'] -unit_dic['omb']
    unit_dic['OmegaM'] = unit_dic['omm']

    return unit_dic

def compute_model(pardic,data_dic):

    '''check for any needed constraints to be applied first'''
    if('constraints' in data_dic.keys()):
        if('omb/omm' in data_dic['constraints']):
            pardic['omb']=pardic['omm']*np.float(data_dic['constraints'].split('=')[-1])
            #print('omb:' , pardic['omb'],pardic['omb']/pardic['omm'])


    #unit sim related info
    unit_dic=data_dic['unit']

    Daz=mcosm.D_A(unit_dic['z'], pardic['H0'], pardic['omm'], 1-pardic['omm'])
    Hz=mcosm.H_z(unit_dic['z'], pardic['H0'], pardic['omm'], 1-pardic['omm'] )
    s8z=mcosm.sigma8z(unit_dic['z'], pardic['H0'], pardic['omm'], 1-pardic['omm'])
    s8z=s8z*pardic['sigma8']

    OMz=mcosm.omMz(unit_dic['z'], pardic['H0'], pardic['omm'], 1-pardic['omm'])
    f=np.power(OMz,0.55)
    fs8= f* s8z

    rs= mcosm.r_s(pardic['omm'],pardic['omb'], pardic['H0'])

    model_dic={'apar':unit_dic['Hz']*unit_dic['rs']/(Hz*rs),
            'aperp':Daz*unit_dic['rs']/(unit_dic['Daz']*rs),
            'fsigma8':fs8}

    return model_dic


def Load_data(cfg,like_name=''):
    summary_file=cfg['summary_file']

    with open(summary_file, 'rb') as file:
        summ= pickle.load(file)
    
    data_dic={}
    temp_par=['apar','aperp','fsigma8']

    cov_key_index={}
    for pp,par in enumerate(summ['cov']['parnames']):
        cov_key_index[par]=pp

    cov_ind=[]
    for par in temp_par:
        data_dic[par]=summ['marg'][par][0]
        cov_ind.append(cov_key_index[par])
   
    print(cov_ind)
   
    ntemp=len(temp_par)
    data_dic['cov']=np.zeros((ntemp,ntemp))
    for ii in range(0,ntemp):
        for jj in range(0,ntemp):
            data_dic['cov'][ii,jj]=summ['cov']['mat'][cov_ind[ii],cov_ind[jj]]

    #inverse covariance
    data_dic['icov']=np.linalg.pinv(data_dic['cov'])
    data_dic['par_order']=temp_par

    #setup unit dic
    data_dic['unit']=unit_pars()


    #check if constraints are consistently set
    if('constraints' in cfg.keys()):
        #copy any constraint if needed
        data_dic['constraints']=cfg['constraints']
        if('omb/omm' in data_dic['constraints']):
            assert(cfg['Parameters']['omb']['fixed']==True)

    return data_dic

def lnlike(pardic,data_dic,cfg):
    '''returns lnlike'''
 
    model_dic=compute_model(pardic,data_dic)

    diff=np.zeros(len(data_dic['par_order']))
    for pp,par in enumerate(data_dic['par_order']):
        diff[pp]=model_dic[par]-data_dic[par]


    chi2=np.dot(diff,data_dic['icov'])
    chi2=np.dot(chi2,diff.T)

    log_like=-0.5*chi2

    return log_like, False

