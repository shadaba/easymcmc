# EASYmcmc

This is a library to handle certain aspect of mcmc so that it is easy to incorporate for any model. All you need is to write a configuration file and provide a python script which loads your data and provide a function which returns loglikelihood. It hides the unneccary documnetation and information progation from model to mcmc.

The power come in the configuration file. There are few needed configuration details but a user can add any other information in config file and will be available to the user module. 

You can learn by examples. Please check the example directory for few examples. But here are the main configuration settings.

 * ### Likelihood :
   you can define any number of likelihood with first things as the name of your new liklihood. 
   Under the name of the likelihood you need two most important information.
     * path : This is the path to the user python module. This modelu needs to have two necessary function.
        first function Load_data(cfg): This function loads the all necessary data while taking the infput as configuration file inputs. It should return all the information needed in a dictionary. This dictionary is called user_data dictionary and will be provided while calling loglikelihood function.
     * logp: second function logp(pardic,user_data_dic,cfg): This should return the log likelihood given the parameter values in the pardic and user_data as all the data you might need to make this calculations. This function also takes cfg as input for any broader context but for most simple scenario it is not used.
     * You can also provide an optional argument called use: (boolean). The default value of use is set to True but if one wants to execlude a particular likelihood then simply set this to False.
 * ### Sampler:
    provide the details of sampler
 * initialize: How should we initilize the starting point of mcmc, currently takes uniform or normal as inputs.


#To test your likelihood simply do
python mcmc_main.py -config_file < your configuration file > -action 2

#To run the mcmc chain use
python mcmc_main.py -config_file < your configuration file > -action 1

#To see other option use
python mcmc_main.py --h

Currently can uses emcee v2.2.1, as will as latest version.
There are some major changes since v2.2.1 which is now incoporated in this repository.

## Examples:
 * gauss2d :
   This shows how to fit a 2 dimensional gaussian
 
 * biaswtheta  
   This fits linear bias to wtheta measurements

 * CosmoTemplate  
   This fits cosmological model with compress parameters such as BAO and rsd (fs8) to get cosmology

 * GP_HOD 
   This require a guassian process and used that to fit HOD model
 
 * SplashBackFit  
   This fits wp with dk14 model to get splash back radius


