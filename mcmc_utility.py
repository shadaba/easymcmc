#Author: Shadab Alam, August 2017
#Find the best fit HODn

from __future__ import print_function

import sys
import os
import time
import yaml

#import triangle
import numpy as np

import importlib.util
from functools import lru_cache
from typing import Dict, Optional

#Try to imports pandas if available
try:
   import pandas as pd
   pd_flag=1
except:
   pd_flag=0
   print('''pandas not found: you can install pandas from
            https://pypi.python.org/pypi/pandas/0.18.0/#downloads
            The code will work without pandas but pandas will speed up loading large text files
            ''')


def ltime(date=False):
    tm=time.localtime()
    datestr=str(tm.tm_mday)+'/'+str(tm.tm_mon)+'/'+str(tm.tm_year)
    tmstr=str(tm.tm_hour)+':'+str(tm.tm_min)+':'+str(tm.tm_sec)

    if(date):
        tmstr=datestr+' '+tmstr

    return tmstr

def timediff(tm1,tm2):
    '''estimates the time difference in seconds'''
    dyear=tm2.tm_year-tm1.tm_year
    dday=tm2.tm_yday-tm1.tm_yday
    dhour=tm2.tm_hour-tm1.tm_hour
    dmin=tm2.tm_min-tm1.tm_min
    dsec=tm2.tm_sec-tm1.tm_sec

    tdiff=(((dyear*365+dday)*24.+dhour)*60+dmin)*60+dsec
    return tdiff

def get_memoryuse(py,unit='MB'):
    if(unit=='MB'):
        return py.memory_info()[0]/2.**20
    elif(unit=='GB'):
        return py.memory_info()[0]/2.**30


def load_txt(fname):
   if(pd_flag==1):
      tb=pd.read_table(fname,sep='\s+',comment='#',header=None,
              dtype=float)
      #tb=tb.as_matrix()
      tb=tb.to_numpy()
   else:
      tb=np.loadtxt(fname)
   return tb

def validate_parameter(cfg_par,par):
    '''This validates whether all the parameters are specified or not properly'''

    props=cfg_par[par].keys()

    if('fixed' not in props):
        cfg_par[par]['fixed']=False
        props=cfg_par[par].keys()

    if(cfg_par[par]['fixed']==True or cfg_par[par]['fixed']==1):
        assert('default' in props), 'parameter=%s set to fixed but default is missing'%(par)
    elif('prior' not in props):
        print('parameter=%s not fixed but prior is missing'%(par))
        sys.exit(-1)
    else:
        #if prior type is not set then set it to uniform
        if('type' not in cfg_par[par]['prior'].keys()):
            cfg_par[par]['prior']['type']='uniform'

        if(cfg_par[par]['prior']['type']=='uniform'):
            assert('min' in cfg_par[par]['prior'].keys() and
                  'max' in cfg_par[par]['prior'].keys()) , 'parameter=%s, prior is uniform but min or max is missing'%par
        else:
            print('prior type = %s for parameter=%s in invalid'%(cfg_par[par]['prior']['type'],par))
            sys.exit()

        if('default' not in cfg_par[par].keys()):
            cfg_par[par]['default']=0.5*(cfg_par[par]['prior']['min']+
                    cfg_par[par]['prior']['max'])

    return cfg_par

def validate_config_file(cfg):
    '''This checks if all the necessary input is provided appropriately'''
    check_dic={'Parameters':{},
            'outroot':'','initialize':'','Likelihood':{}
               }
    for cc,cp in enumerate(check_dic.keys()):
       if(cp not in cfg.keys()):
          print('Missing necessary configuration: %s'%(cp))
          sys.exit()

       assert(type(check_dic[cp])==type(cfg[cp])), 'The config file has wrong type for: %s '%(cp)

    return

def setup_sampler(cfg):
    '''This check and populate sampler details'''
    sampler_defaults={'emcee':{'nwalkers':200,'nsteps':100},
                      'zeus':{'nwalkers':200,'nsteps':100}
            }

    if('Sampler' not in cfg.keys()):
        cfg['Sampler']={'name':'emcee'}

    if(cfg['Sampler']['name'] not in sampler_defaults.keys()):
        raise ValueError ('invalid sampler')

    tname=cfg['Sampler']['name']
    for pp in sampler_defaults[tname].keys():
        if(pp not in cfg['Sampler'].keys()):
           cfg['Sampler'][pp]=sampler_defaults[tname][pp]

    #obtain sampling dimension

       #update the parameter list
    cfg['Sampler']['ndim']=sampling_dimension(cfg,verbose=cfg['verbose'])

    return cfg

def fill_default_config(cfg):
    '''Set the default values'''
    defaults_dic={'save_model':False, 'bfit':False,
            'nthreads': 8}

    for pp in defaults_dic.keys():
        if(pp not in cfg.keys()):
            cfg[pp]=defaults_dic[pp]
            if(cfg['verbose']):
                print('Setting: %s ='%pp,cfg[pp])

    #if Likelihood use is not set then set it to True and if it is False then pop it
    red_key=[]
    for ll, like_name in enumerate(cfg['Likelihood'].keys()):
        if('use' not in cfg['Likelihood'][like_name]):
            cfg['Likelihood'][like_name]['use']=True

        if(cfg['Likelihood'][like_name]['use']==False):
            red_key.append(like_name)

    #remove redundant likelihood
    cfg['redundant_Like']={}
    for like_name in red_key:
        cfg['redundant_Like'][like_name]=cfg['Likelihood'].pop(like_name)

    return cfg

def init_config_yaml(inifile='',verbose=None):
    '''This loads a yaml file and set the configuration'''

    with open(inifile) as file:
       # The FullLoader parameter handles the conversion from YAML
       # scalar values to Python the dictionary format
       cfg = yaml.load(file, Loader=yaml.FullLoader)

    #apply conditional interpolation if needed
    if(cfg['Cinterpolation']):
        import conditional_string_interpolation as csi
        cfg=csi.interp_dic(cfg,child_dic={},verbose=cfg['verbose'])
   
    if(verbose is not None):
        cfg['verbose']=verbose

    validate_config_file(cfg)

    #This expands the parameters if multiples of them are needed
    cfg=expand_parameters(cfg)

    #convert to my parlist language
    cfg['parlist']=list(cfg['Parameters'].keys())

    #now validate the input of each parameter
    for pp,par in enumerate(cfg['parlist']):
        cfg['Parameters']=validate_parameter(cfg['Parameters'],par)


    #sampler details
    cfg=setup_sampler(cfg)

    #some variable if not set in config file then use default
    cfg=fill_default_config(cfg)

    #finally create a dictionary with all the parameters for local use
    cfg['LD']={}
    for pp,par in enumerate(cfg['parlist']):
        cfg['LD'][par]=cfg['Parameters'][par]['default']

    return cfg

def expand_parameters(cfg):
    '''This reads the nbin value and create so many iterations of the parameters'''

    cpars=cfg['Parameters']
    parlist=list(cpars.keys())
    for pp,par in enumerate(parlist):
        if('numpar' not in cpars[par].keys()):
            continue
        #if numpar is given then  change the parameters accordingly
        for ii in range(0,cpars[par]['numpar']):
            tpar='%s_%d'%(par,ii)
            cfg['Parameters'][tpar]=cfg['Parameters'][par].copy()
            for tt,tkey in enumerate(cfg['Parameters'][par].keys()):
                if(tkey=='numpar'):
                    cfg['Parameters'][tpar].pop(tkey)
                elif(isinstance(cfg['Parameters'][par][tkey],list) or 
                     isinstance(cfg['Parameters'][par][tkey],np.ndarray)):
                    cfg['Parameters'][tpar][tkey]=cfg['Parameters'][par][tkey][ii]
                    
        #pop the original parameter
        cfg['Parameters'].pop(par)

    
    return cfg


def sampling_dimension(cfg,verbose=True):
    '''Computes thr dimensionality fir sampling'''
    ndim=0
    if(verbose):
        print('*****Using parameters *****')
    for pp in cfg['parlist']:
        if(cfg['Parameters'][pp]['fixed']==False):
            ndim=ndim+1
            if(verbose):
                print(pp)#, SHOD[pp][1],SHOD[pp][2])
    if(verbose):
        print('**** ndim= %d ****'%ndim)

    return ndim

def test_likelihood(cfg,nlike=4):
    #SHOD['nhocells']=(nlike*50)
    cfg['nlike']=nlike
    fout_name=cfg['outroot']+'.test.like.global.%d'%nlike
    fout=open(fout_name,'w')

    localDic=cfg['LD'].copy()

    tm1=time.localtime()
    fout.write('####BEGIN  : '+str(tm1.tm_hour)+':'+str(tm1.tm_min)+':'+str(tm1.tm_sec)+'\n')
    for ll, like_name in enumerate(cfg['Likelihood'].keys()):
        loglike,blob=cfg['Likelihood'][like_name]['module'].lnlike(localDic,
                cfg['Likelihood'][like_name]['data'],cfg)
        tm2=time.localtime()
        print(like_name,loglike,timediff(tm1,tm2))
        res_str='%s loglike: %12.5f, dt=%s\n'%(like_name,loglike,timediff(tm1,tm2))
        fout.write(res_str)
        tm1=tm2

    #write out the dictionary
    print_dictionary(fout,cfg,dicname='',indent='')
    fout.close()

    print('check for test of global likelihood: ',fout_name)

    if(nlike>1):
       nlike=nlike-1
       test_likelihood(cfg,nlike=nlike)
    else:
       sys.exit()
    return 0

def print_dictionary(fout,dicin,dicname='HOD',indent=''):
    #The print everything in dictionary except array too big
    fout.write(indent+'###dictionary=%s###\n'%(dicname))
    for ii,tkey in enumerate(dicin.keys()):
        if(isinstance(dicin[tkey],dict)): #check if its dictionary then make a recursive call
            print_dictionary(fout,dicin[tkey],dicname=dicname+'>'+tkey,indent=indent+'   ')
        elif(isinstance(dicin[tkey],np.ndarray)): #if its an array
            #get the array shape
            tshape=dicin[tkey].shape
            if(type(tkey)==float):
                tshapestr=str(tkey)+' : '
            else:
                tshapestr=tkey+' : '
            for ss in tshape:
               tshapestr='%s %d'%(tshapestr,ss)
            fout.write(indent+tshapestr+'\n')
            #if size less than 100 then print all elements
            if(dicin[tkey].size<100):
               if(None not in dicin[tkey]):
                  np.savetxt(fout,dicin[tkey],fmt='%12.8f')
               else:
                  for tval in dicin[tkey]:
                     fout.write("{} ".format(tval))
                  fout.write('\n')
            else: #if size more than 100 then print shape and few elements
               tmparr=dicin[tkey].reshape(dicin[tkey].size)
               tstr1='* First Five:'
               tstr2='* Last  Five:'
               for tt in range(0,5):
                   tstr1="{} {}".format(tstr1,tmparr[tt])
                   tstr2="{} {}".format(tstr2,tmparr[-tt])
               fout.write(indent+tstr1+'\n')
               fout.write(indent+tstr2+'\n')
        elif(isinstance(dicin[tkey],list)): #if list then print the list
            fout.write(indent+tkey+' : ')
            for tmem in dicin[tkey]:
               if(isinstance(tmem,str)):
                   fout.write('%s '%tmem)
               else:                   fout.write('%f '%tmem)
            fout.write('\n')
        elif(isinstance(dicin[tkey],str)): #if its a string
            fout.write('%s %s : %s\n'%(indent,tkey,dicin[tkey]))
        else: #otherwise its either a integet or float            try:
            try:
               fout.write('%s %s : %f\n'%(indent,tkey,dicin[tkey]))
            except:
               if(tkey not in ['int_r200', 'int_rsr200']):
                  print('** cant write it in output file:',tkey,dicin[tkey])

    return 0

def initialize_par(SHOD,ndim,nwalkers,emcee_version='2.2.1'):
   ''' This initialize without uniform distribution between the limits given in config file'''

   low=np.zeros(ndim)
   high=np.zeros(ndim)

   ploc=np.zeros(ndim)
   psig=np.zeros(ndim)

   ii=0
   for pp in SHOD['parlist']:
      tdic=SHOD['Parameters'][pp]
      if(tdic['fixed']==False):
         if(tdic['prior']['type']=='uniform'):
            low[ii]=tdic['prior']['min']
            high[ii]=tdic['prior']['max']
            ploc[ii]=tdic['default']
            psig[ii]=(-low[ii]+high[ii])/100.0
         elif(tdic['prior']['type']=='normal'):
            ploc[ii]=tdic['prior']['mean']
            psig[ii]=tdic['prior']['sigma']
            low[ii]=ploc[ii]-5*sigma
            high[ii]=ploc[ii]+5*sigma
         ii=ii+1

   diff=high-low
   
   if(emcee_version<='2.2.1'):
      pos0 = [np.random.random(ndim) for i in range(nwalkers)]
      for ii in range(0,nwalkers):
         if(SHOD['initialize']=='uniform'):
            pos0[ii]=diff*pos0[ii]+low
         elif(SHOD['initialize']=='normal'):
            for jj in range(0,ndim):
               pos0[ii][jj]=np.random.normal(loc=ploc[jj],scale=psig[jj])
   else:#(emcee_version<='2.2.1'):
      pos0 = np.random.random((nwalkers,ndim))
      for ii in range(0,nwalkers):
         if(SHOD['initialize']=='uniform'):
            pos0[ii,:]=diff*pos0[ii]+low
         elif(SHOD['initialize']=='normal'):
            for jj in range(0,ndim):
               pos0[ii,jj]=np.random.normal(loc=ploc[jj],scale=psig[jj])


   return pos0



#This function is used in emcee
def lnprob(theta, SHOD,tmp={'name':'empty','empty':'normal emcee'}):
    '''tmp is a dictionary with one entry as name and other entry as values with the name key
    Currently it is only used for importance sampling'''

    #To store all the things which gets updated every iteration for parallel mcmc consideration
    #localDic= collections.OrderedDict()
    localDic=SHOD['LD'].copy() #copy the halo population stuff
    localDic[tmp['name']]=tmp[tmp['name']]

    localDic=map_param(theta,SHOD,localDic)
    lp = lnprior(SHOD,localDic)
    if not np.isfinite(lp):
        blob=np.zeros(SHOD['blobsize'])
        return -np.inf ,blob


    # print(theta[0],SHOD['LM1'][0],lnlike(SHOD),lp)
    #also return number of galaxies
    loglike=lp;#blob=[]
    for ll, like_name in enumerate(SHOD['Likelihood'].keys()):
        tmodule=get_user_module(SHOD['Likelihood'][like_name]['module_name'])
        lnlike, blob =tmodule.lnlike(localDic,
                      SHOD['Likelihood'][like_name]['data'],SHOD)
        loglike=loglike+lnlike

    return loglike, blob


# Define the probability function as likelihood * prior.
def map_param(theta,SHOD,LD): #LD is a local ditionary which should store necessary stuff for updates in mcmc
   jj=0
   for ii,par in  enumerate(SHOD['parlist']):
      if(SHOD['Parameters'][par]['fixed']):
         LD[par]=SHOD['Parameters'][par]['default']
      else:
         LD[par]=theta[jj]
         jj=jj+1

   return LD

def lnprior(SHOD,LD):
    '''Applies the priors'''

    for pp in SHOD['parlist']:
        td=SHOD['Parameters'][pp]
        if(td['fixed']):
            continue
        elif(td['prior']['type']=='uniform'):
            if(td['prior']['min']>LD[pp] or td['prior']['max']<LD[pp]):
                return -np.inf
        elif(td['prior']['type']=='normal'):
            #estimate the normal distribution values
            print('!!!Finish estimating normal distribution prior')
            sys.exit()
            return 

    return 0.0

def initiate_output_files(SHOD,args):
   '''This sets up the files to be written during the long runs'''

   if(args.action==1 or args.action==5):
      #action=1 run mcmc this is chain file
      #action=5 run importance sampling
      chainfile=SHOD['outroot']+'.txt'
   else:
      print('This should not be called here, something went wrong')
      print('This function should be called only for mcmc run or importance sampling')
      sys.exit()

   if(os.path.isfile(chainfile)):
      print('The chain file already exists (exiting): %s'%chainfile)
      sys.exit()
   else:
      #initiate chain file
      fchain=open(chainfile,'w')
      if(args.action==1):
         labs='#pars: weight, loglike '
      elif(args.action==5):
         labs='#pars: index, IMPloglike, loglike '
      else:
         print('This should not be called here, something went wrong')
         print('This function should be called only for mcmc run or importance sampling')
         sys.exit()

      jj=0
      for pp in SHOD['parlist']:
         if(SHOD['Parameters'][pp]['fixed']==False):
            labs=labs+', '+pp
            jj=jj+1

      #also writes derived parameter
      if('blob_key' in SHOD.keys()):
          labs=labs+', %s'%(SHOD['blob_key'])
      else:
          labs=labs+', blob'

      #also write stdvvf
      if(SHOD['save_model']==True):
          labs=labs+', seed*'
      fchain.write(labs+'\n')
      fchain.close()
      print('Created Chain file: ',chainfile)
      #initiate parameter file
      create_parfile(SHOD)
            #if model needs to be saved then initiate that as well
      if(SHOD['save_model']==True):
         SHOD['model_root']=SHOD['outroot']+'.model'
         SHOD['cur_model']=-1
         SHOD['nmodel_thisfile']=0
         #save stat_index
         with open(SHOD['model_root']+'.stat_index','w') as ftmp:
             ftmp.write('#quantity, model_index, fit\n')
             for tt,tstat in enumerate(SHOD['stat_index'].keys()):
                if(tstat!='wpxi024'):
                   ftmp.write('%s %d %d\n'%(tstat,SHOD['stat_index'][tstat],SHOD[tstat+'_fit']))
                else:
                   ftmp.write('%s %d 1\n'%(tstat,SHOD['stat_index'][tstat]))
             print('model stat index is written to file:\n'+SHOD['model_root']+'.stat_index')

      #This should be changed if the lnlike is return type is updated
      if(SHOD['save_model']==True):
          SHOD['blobsize']=SHOD['data']['nmodel']+2 #So that sometimes invalid pars also returns the right size blob
      else:
          SHOD['blobsize']=1

   return SHOD, chainfile

#This function is to create auxiliary files
def create_parfile(SHOD):
   #Initiate the files-names
   parfile=SHOD['outroot']+'.paramnames'
   rangefile=SHOD['outroot']+'.ranges'
   def_value_file=SHOD['outroot']+'.defpar'
   #initiate file pointer
   fpar=open(parfile,'w')
   frange=open(rangefile,'w')
   fdef=open(def_value_file,'w')
   jj=0
   for pp in SHOD['parlist']:
      tdic=SHOD['Parameters'][pp]
      fdef.write('%s  %10.5f\n'%(pp,tdic['default']))
      if(tdic['fixed']==False):
         fpar.write('%s %s\n'%(pp,pp))
         tline='%20s '%pp
         for tprior in ['type', 'min','max','mean','sigma']:
             if(tprior in tdic['prior'].keys()):
                 if(type(tdic['prior'][tprior])==str):
                     tline='%s %10s'%(tline,tdic['prior'][tprior])
                 else:
                     tline='%s %12.5f'%(tline,tdic['prior'][tprior])

         frange.write(tline+'\n')
      else:
         tline='%20s %10s %12.5f %12.5f'%(pp,'fixed',tdic['default'],tdic['default'])

   if(SHOD['save_model']==1):
      fpar.write('seed* r_{seed}\n')

   #close the files
   fpar.close()
   print('Created par file file: ',parfile)
   frange.close()
   print('Created range file file: ',rangefile)
   fdef.close()
   print('Created def par file:',def_value_file)
   return 0

def chainsTostats(SHOD,chainfile):
   #load the chains file
   load_chain=load_txt(chainfile)
   ####################################################
   burn=int(load_chain.shape[0]*0.2)
   load_chain=load_chain[burn:,:]

   #The last column is -logl ike, find mimimum to get best fit model
   indbfit=np.argmax(load_chain[:,1])
   #
   #Now write out some statistics
   outfile=SHOD['outroot']+'.stats'
   fout=open(outfile,'w')
   fout.write('#par, mean, std, bfit(max -loglike), percentile(50, 16,84, 2.5,97.5,0.15,99.85)\n')
   jj=0
   for ii,par in enumerate(SHOD['parlist']):
      if(SHOD['Parameters'][par]['fixed']): #the parameter is fixed
         continue

      mean=np.average(load_chain[:,jj+2],weights=load_chain[:,0])
      mu2=np.average(np.power(load_chain[:,jj+2],2),weights=load_chain[:,0])
      sigma=np.sqrt(mu2-mean*mean)
      labs='%10s %12.6f %12.6f %12.6f'%(par,mean,
               sigma,load_chain[indbfit,jj+2])
      
      tmpper=np.percentile(load_chain[:,jj+2],[50,16,84,2.5,97.5,0.15,99.85])
      for perc in tmpper:
         labs='%s %12.6f'%(labs,perc)
      fout.write(labs+'\n')
      jj=jj+1
   fout.close()
   print('written stats: ',outfile)

   mfitpars=np.mean(load_chain[:,2:],axis=0)
   bfitpars=load_chain[indbfit,2:]
   #####################################################
   return bfitpars,mfitpars


def BestFit(SHOD,bfitpars,tag='',xispace=['Real','zRSD']):
   print('********************\nEvaluating Best fit results...',ltime())
   if(SHOD['bfit']==False): #This means mcmc chains are ran
      SHOD['bfit']=True
   SHOD['tag']=tag

   outroot_store=SHOD['outroot']
   for ix,xspace in enumerate(xispace):
       #wite the bestfit model data and error
       outfile=SHOD['outroot']+'%s.chi2'%(xspace)
       fout=open(outfile,'a')
       LD=SHOD['hpop'].copy()
       LD['xispace']=xspace
       LD['outroot']=outroot_store+'_%s%s.gcat'%(tag,xspace)
       SHOD['outroot']=outroot_store+'%s_%s'%(tag,xspace)
       #maximum likelihood model
       jj=0
       for ii,pp in enumerate(SHOD['parlist']):
           if(SHOD[pp][3]>=0):
               LD[pp]=bfitpars[jj]
               jj=jj+1

       loglike,blob=lnlike(SHOD,LD)
       SHOD['chi2_Mfit']=-2.0*loglike
       dof=SHOD['data']['icov'].shape[0]-SHOD['ndim']+1
       fout.write('#chi2 %s fit model: %8.4f , dof= %d\n'%(tag,SHOD['chi2_Mfit'],dof))
       parstr='%s pars '%tag
       for tkey in SHOD['parlist']:
           if(LD[tkey]!=None):
              parstr='%s %s = %12.6f '%(parstr,tkey,LD[tkey])
           else:
              parstr='%s %s = None '%(parstr,tkey)
       fout.write(parstr+'\n')

       fout.close()
       print('written: ',outfile,ltime())

   SHOD['outroot']=outroot_store

   return SHOD


class ModuleRegistry:
    """
    Singleton class to manage multiple user modules
    """
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(ModuleRegistry, cls).__new__(cls)
            cls._instance._modules = {}
        return cls._instance

    def __init__(self):
        # Initialize only if this is the first time
        if not hasattr(self, '_modules'):
            self._modules = {}

    def get_module(self, module_name: str):
        """Get a loaded module by name"""
        if module_name not in self._modules:
            raise KeyError(f"Module '{module_name}' not initialized. Available modules: {list(self._modules.keys())}")
        return self._modules[module_name]

    def set_module(self, module_name: str, module):
        """Set a module in the registry"""
        self._modules[module_name] = module

    def clear(self):
        """Clear all modules"""
        self._modules.clear()

    def list_modules(self):
        """List all available module names"""
        return list(self._modules.keys())

@lru_cache(maxsize=128)
def load_user_module(module_path: str):
    """
    Load user module once and cache it.
    Returns the loaded module.
    """
    try:
        module_name = module_path.split('/')[-1].replace('.py', '')
        spec = importlib.util.spec_from_file_location(module_name, module_path)
        if spec is None:
            raise ImportError(f"Could not load specification for module: {module_path}")

        module = importlib.util.module_from_spec(spec)
        sys.modules[module_name] = module
        spec.loader.exec_module(module)
        return module
    except Exception as e:
        raise ImportError(f"Failed to load module {module_path}: {str(e)}")

def initialize_user_modules(module_config: Dict[str, str]):
    """
    Initialize multiple user modules from a configuration dictionary.
    Args:
        module_config: Dictionary mapping module names to their file paths
    """
    registry = ModuleRegistry()
    for module_name, module_path in module_config.items():
        module = load_user_module(module_path)
        registry.set_module(module_name, module)

def get_user_module(module_name: str):
    """
    Get a specific user module by name.
    """
    return ModuleRegistry().get_module(module_name)

def compute_with_user_module(data, module_name: str, function_name: str, *args, **kwargs):
    """
    Generic function to call any function from a specific user module
    """
    module = get_user_module(module_name)
    if not hasattr(module, function_name):
        raise AttributeError(f"Function '{function_name}' not found in module '{module_name}'")
    func = getattr(module, function_name)
    return func(data, *args, **kwargs)
