#Author: Shadab Alam, October 2019
#This dumps and load dictionary in system and python version independent way
#It stores dictionary recursively
#It stores numpy arrays in fits files
#It stores strongs in normal text file
#currently any other file format like list etc is not supported

import fitsio as F
import numpy as np
import os

def update_keylist(indic,fname,verbose=0):
    '''Loads and compare the dictionary and return only the updated list'''
    
    if(verbose>2):
        print('loading existing dictionary')
    this_dic=dump_load_dic_fits(fname,verbose=verbose,tabin=' ',nodic=1)
    keylist=[]
    for tt,tkey in enumerate(indic.keys()):
        this_name=fname+'/'+tkey
        rmfile=''; pr_msg=''
        if(tkey not in this_dic.keys()  or type(indic[tkey])==dict):
            keylist.append(tkey)
            pr_msg='%s :new key'%(tkey)
        elif(type(indic[tkey])==str):
            if(indic[tkey]!=this_dic[tkey]):
                keylist.append(tkey)
                rmfile=this_name
                pr_msg='%s need updating'%(tkey)
        elif(type(indic[tkey])==np.ndarray):
            if(np.sum(indic[tkey]==this_dic[tkey])!=indic[tkey].size):
                keylist.append(tkey)
                pr_msg='%s need updating'%(tkey)
                rmfile=this_name+'.fits'
                
        if(verbose>0):
            print(pr_msg)
            
        if(rmfile!=''):
            os.remove(rmfile)
            if(verbose>0):
                print('removed: %s'%rmfile)
    
    return keylist

def dump_save_dic_fits(indic,fname,append=0,verbose=1,tabin='',usefits=1):
    '''This saves the distionary in directory structures
    only saves array and strings
    when usefits is set to 1 then arrays are saved with fits file
    otherwise it is saved with np.save
    '''
    
    keylist=indic.keys()
    
    #create a diretory
    #make sure the directory doesn't exists already
    if(os.path.isdir(fname)):
        if(append==0):
            print('directory exists and not appending: quitting')
            return
        elif(verbose>2):
            keylist=update_keylist(indic,fname,verbose=verbose)
            if(len(keylist)==0):
                print('directory up to date')
            else:
                print('directory exists: appending: ',keylist)
            
    else:
        os.mkdir(fname)
        if(verbose>2):
            print(tabin+fname)
    
    #go over dictionary and save things either in fits or text file
    for tt,tkey in enumerate(keylist):
        print_tkey=0
        this_name=fname+'/'+tkey
        if(type(indic[tkey])==dict):
            #make sure it is not pointing to the parent dictionary
            if(indic[tkey]==indic):
                if(verbose>2):
                    print(tabin+' %s dictionary inside same dictionary will lead to infinte loop ignoring'%tkey)
                    print_tkey=1
                continue
            else:
                dump_save_dic_fits(indic[tkey],this_name,append=append,verbose=verbose,tabin=tabin+'   ')
        elif(type(indic[tkey])==str):
            open(this_name,'w').write(indic[tkey])
        elif(type(indic[tkey])==np.ndarray and usefits==1):
            #open and save a fits file
            if(len(indic[tkey].shape)==1):
                outtype=[(tkey,indic[tkey].dtype)]
                dout=np.zeros(indic[tkey].size,dtype=outtype)
            else:
                outtype=[(tkey,indic[tkey].dtype,indic[tkey].shape)]
                dout=np.zeros(1,dtype=outtype)
            dout[tkey]=indic[tkey]
            fhandle=F.FITS(this_name+'.fits','rw')
            fhandle.write(dout)
            fhandle[-1].write_checksum()
            fhandle.close()
        elif(type(indic[tkey])==np.ndarray and usefits!=1): #save array with np.save
            arrayfile=this_name+'.nparray'
            np.save(arrayfile,indic[tkey])
        elif(type(indic[tkey]) in [type(1),type(1.0),type(np.float64(1.0))]):
            outname=fname+'/numbers.txt'
            if(type(indic[tkey])==type(1)):
                open(outname,'a').write('%s,%d,%s\n'%(tkey, indic[tkey],type(indic[tkey])))
            else:
                open(outname,'a').write('%s,%f,%s\n'%(tkey, indic[tkey],type(indic[tkey]))) 
        elif(verbose>2):
            print('%s is of type:%s and cant be saved'%(tkey,type(indic[tkey])))
            print_tkey=1
        
        if(print_tkey==0 and verbose>2):
            print('%s %s %s'%(tabin,tkey,type(indic[tkey])))
            
            
    return
 
def dump_load_dic_fits(fname,verbose=0,tabin='',nodic=0):
    '''This loads the dictionary the dictionary in directory structures
    only loads array and strings'''
    
    #create a diretory
    #make sure the directory doesn't exists already
    if(not os.path.isdir(fname)):
        if(verbose>0):
            print('directory does not exists: quiting') 
        return {'info':'does not exists'}
    

    if(verbose>1):
        print(tabin+fname)
      
    outdic={}
    #check the list of thing in the file
    keylist=os.listdir(fname)

    #go over list of file and load things either in fits or text file
    for tt,tkey in enumerate(keylist):
        #print_tkey=0
        this_name=fname+'/'+tkey
        #print(this_name)
        
        #decide between file and directory
        if(not os.path.isfile(this_name)):#then call again
            if(nodic==0):
                outdic[tkey]=dump_load_dic_fits(this_name,verbose=verbose,tabin=tabin+'   ')
            type_key=dict
        elif(this_name[-5:]=='.fits'): #load numpy array from fits file
            with F.FITS(this_name) as fin:
                outdic[tkey[:-5]]=fin[1][tkey[:-5]][:]
                if(len(outdic[tkey[:-5]].shape)>1):
                    outdic[tkey[:-5]]=outdic[tkey[:-5]][0]
            type_key=outdic[tkey[:-5]].dtype
        elif(this_name[-12:]=='.nparray.npy'): #load numpy array from numpy.save files
            outdic[tkey[:-12]]=np.load(this_name)
            type_key=outdic[tkey[:-12]].dtype
        elif(this_name[-11:]=='numbers.txt'):
            tlines=open(this_name,'r').readlines()
            for tii,tline in enumerate(tlines):
               tspl=tline[:-1].split(',')
               if('int' in tspl[2]):
                   outdic[tspl[0]]=np.int(tspl[1])
               elif('float64' in tspl[2]):
                   outdic[tspl[0]]=np.float64(tspl[1])
               elif('float' in tspl[2]):
                   outdic[tspl[0]]=np.float(tspl[1])
               else:
                   outdic[tspl[0]]=tspl[1]
            type_key='numbers'
        else: #load the text file
            outdic[tkey]=open(this_name,'r').readlines()[0]
            type_key=str
            
        if(verbose>2):
            print('%s %s %s'%(tabin,tkey,type_key))
            
    return outdic

