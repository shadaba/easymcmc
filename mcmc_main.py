#Author: Shadab Alam, August 2017
#Find the best fit HODn

from __future__ import print_function

import os
#This is temporary solution
#https://stackoverflow.com/questions/51256738/multiple-instances-of-python-running-simultaneously-limited-to-35?noredirect=1&lq=1
#os.environ['OPENBLAS_NUM_THREADS'] = '32'

#import triangle
import numpy as np
import scipy.optimize as op

import sys

#fitsio for saving model
#import fitsio as F

#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as pl

import argparse
import time
import mcmc_utility as mcutil

import importlib

from typing import Dict,List, Any

import multiprocessing as mprocess

if __name__=="__main__":
   parser = argparse.ArgumentParser(description='Easy to run mcmc sampling:')
   #these should be input
   parser.add_argument('-config_file',default='test.ini')

   #parser.add_argument('-outroot' ,default='test/test')
   parser.add_argument('-burnin' ,type=int,default=10)
   #parser.add_argument('-nwalker',type=int,default=24)
   #parser.add_argument('-nsteps',type=int,default=5000)
   parser.add_argument('-action',type=int,default=2,help='''
             action=1 run chains, \n 
             action=2 test likelihoood, \n 
             action=3 get the parameters variation, if stats file exists then it will load it and use the bestfit parameter as the fixed point for parameter dependence\n 
             action=4 minimize using scipy optimize\n
             action=5 importance sampling. This will require importance parameters in ini file''')
   parser.add_argument('-nthreads',type=int,default=-1,help='''number of threads for emcee. if this is -1 then the value in ini file is used otherwise value from here is used, The nthreads is passed to paircounter.''')
   parser.add_argument('-storetime',type=int,default=0,help='''set 1 to store time \n''')

   args = parser.parse_args()

   #get the pid of this process
   import psutil
   pid = os.getpid()
   py = psutil.Process(pid)
   time_beg=time.localtime()
   print('my pid=',pid,mcutil.ltime())

   #import multiprocessing if needed
   #elif(args.action==5):
   #   # Start Ray.
   #   #import ray
   #   #ray.init()


def worker_init(module_config: Dict[str, str]):
    """
    Initialization function for worker processes
    """
    mcutil.initialize_user_modules(module_config)


def load_user_module(cfg):

    cfg['user_modules']={}
    for ll, like_name in enumerate(cfg['Likelihood'].keys()):
        module_path=cfg['Likelihood'][like_name]['path']
        module_name= module_path.split('/')[-1][:-3]
        
        cfg['user_modules'][module_name]=module_path
        cfg['Likelihood'][like_name]['module_name']=module_name
        #spec = importlib.util.spec_from_file_location(module_name, file_path)
        #cfg['Likelihood'][like_name]['module'] = importlib.util.module_from_spec(spec)
        #sys.modules[module_name] = cfg['Likelihood'][like_name]['module']
        #spec.loader.exec_module(cfg['Likelihood'][like_name]['module'])
    
    #initialize the user module in main
    mcutil.initialize_user_modules(cfg['user_modules'])
    
    for ll, like_name in enumerate(cfg['Likelihood'].keys()):
        #get the module
        tmodule=mcutil.get_user_module(cfg['Likelihood'][like_name]['module_name'])
        #load the data
        print('*** Loading the user data files: %s'%cfg['Likelihood'][like_name]['module_name'])
        cfg['Likelihood'][like_name]['data']=tmodule.Load_data(cfg,like_name=like_name)

    return cfg

if __name__=="__main__":
   print('current memory use in MB: ',mcutil.get_memoryuse(py))
   print('time since begin: ',mcutil.timediff(time_beg,time.localtime()))
   
   #Read config file
   tm=time.localtime()
   tstr=str(tm.tm_hour)+':'+str(tm.tm_min)+':'+str(tm.tm_sec)
   print('#### BEGIN script : %s %s '%(args.config_file,tstr))
   print('Step1: Initializing Config file')
   cfg=mcutil.init_config_yaml(inifile=args.config_file)

   #load the user-defined llibrary
   cfg= load_user_module(cfg)
   
   
   use_mpi=0
   #figure out sampler related imports
   if(cfg['Sampler']['name']=='emcee'):
       import emcee
       emcee_version=emcee.__version__
       print(' using emcee as smapler with version : %s'%(emcee_version))
       #first create the mpi pool
       if(args.action==1):
           pool=mprocess.Pool(args.nthreads,initializer=worker_init, initargs=(cfg['user_modules'],))
           #try:
           #    from emcee.utils import MPIPool
           #    pool = MPIPool(debug=False)
           #    use_mpi=1
           #except:
           #    print('No mpi available not using mpi pool')
           #    use_mpi=0
   elif(cfg['Sampler']['name']=='zeus'):
       import zeus
       print(' using zeus as smapler')
       pool=mprocess.Pool(args.nthreads,initializer=worker_init, initargs=(cfg['user_modules'],))

   if(use_mpi==1):
      # Wait for instructions from the master process.
      if not pool.is_master():
         #print('not master rank:',pool.rank)
         pool.wait()
         sys.exit(0)


   

   # Reproducible results!
   ranseed=123
   np.random.seed(ranseed)
   print('initializing random seed: ',ranseed)


   #if time needs to be stored
   if(args.storetime==1):
       file_time=SHOD['outroot']+'.time.memory'
       print('writing time and memory in file:',file_time)
       with open(file_time,'w') as ftime:
          ftime.write('#Time in sec, memomry in MB\n')
          ftime.write('%f %f\n'%(mcutil.timediff(time_beg,time.localtime()),mcutil.get_memoryuse(py)))


   #if action==2 then just test likelihood and exit
   if(args.action==2):
       print('Step3: Testing Likelihoods: %s'%mcutil.ltime())
       mcutil.test_likelihood(cfg,nlike=1)
       #test and exit otherwise just run the chains

   #if action ==3 then evaluate the clustering as the function of one parameter
   if(args.action==3):
       print('Step3: Parameter dependence: %s'%mcutil.ltime())
       Param_dependence(SHOD)

   #if action ==4 then run scipy minimizer
   if(args.action==4):
       print('Step3: Running scipy minimzer: %s'%mcutil.ltime())
       mcutil.run_minimizer(SHOD)

   if(args.action==5):
       print('Step3: Importance sampling: %s'%mcutil.ltime())
       mcutil.run_importance_sampling(SHOD,nsample=100000)

   #run mcmc only if bfit=0 otherwise directly load chains
   if(cfg['bfit']==False):
      #now prepare for mcmc
      # Set up the sampler.
      ndim, nwalkers, nsteps = cfg['Sampler']['ndim'], cfg['Sampler']['nwalkers'], cfg['Sampler']['nsteps']
      #initialize the point
      pos0=mcutil.initialize_par(cfg,ndim,nwalkers,emcee_version=emcee_version)
      
      print('*** ndim, nwalkers, nsteps:',ndim,nwalkers,nsteps)
     

      #decide threading
      if(args.nthreads>0):
         nthreads=args.nthreads
         cfg['nthreads']=args.nthreads
      else:
         nthreads=cfg['nthreads']

      #initialize the sampler
      if(cfg['Sampler']['name']=='emcee'):
          if(use_mpi==1):
              sampler = emcee.EnsembleSampler(nwalkers, ndim, mcutil.lnprob,args=[cfg],pool=pool)
          else:
              sampler = emcee.EnsembleSampler(nwalkers, ndim, mcutil.lnprob,args=[cfg],pool=pool)
          
          def mcmc_call(pos,nsteps):
              if(emcee_version<='2.2.1'):
                  return sampler.sample(pos, iterations=nsteps,storechain=False)
              else:
                  return sampler.sample(pos, iterations=nsteps,store=False)
      elif(cfg['Sampler']['name']=='zeus'):
          sampler=zeus.EnsembleSampler(nwalkers, ndim, mcutil.lnprob,args=[cfg])
          
          def mcmc_call(pos,nsteps):
              return sampler.sample(pos, iterations=nsteps,progress=False)
          
      #Initiate the chain files
      cfg, chainfile =mcutil.initiate_output_files(cfg,args)

      #####################################################
      # Clear and run the production chain.
      print("Started Running MCMC... %s"%mcutil.ltime())
      if(cfg['Sampler']['name']=='emcee'):
          print("burnin: %d"%args.burnin)
          pos, prob, state,blob =sampler.run_mcmc(pos0, args.burnin)
          sampler.reset()
          pos0=pos

      print("\n\nProduction MCMC... %s"%mcutil.ltime())#tstr)


      if(0): #simple run
         sampler.run_mcmc(pos, nsteps)# rstate0=np.random.get_state())
         np.savetxt('tmp.txt',sampler.chain.flatten())
         print('done')
         sys.exit()
      if(1): #Advance run print progress bar
         width=0.05;#0.20;
         fact=1.0/nsteps;
         wnow=0; nchain_pt=0
         for ss, result in enumerate(mcmc_call(pos0,nsteps)):#sampler.sample(pos, iterations=nsteps,storechain=False)):
            fchain=open(chainfile,'a')
            if(cfg['save_model']==0):
                if(cfg['Sampler']['name']=='emcee'):
                    np.savetxt(fchain,np.column_stack([np.ones(nwalkers),result[1],result[0],result[3]]),fmt='%12.8e')
                elif(cfg['Sampler']['name']=='zeus'):
                    np.savetxt(fchain,np.column_stack([np.ones(nwalkers),result[1],result[0]]),fmt='%12.8e')
            else:
                #assumes result[3] first column is Ngal, second is random seed and  rest is model
                blob_arr=np.array(result[3])
                #print('blob_arr',blob_arr.shape)
                #print('res3: ',len(result[3]),result[3])

                np.savetxt(fchain,np.column_stack([np.ones(nwalkers),result[1],result[0],blob_arr[:,:2]]),fmt='%12.8e')

                #udate the model fil
                cfg=update_modelfile(cfg,blob_arr,index=1)

            #for kk in range(result[0].shape[0]):
            #    fchain.write("{0:4d} {1:s}\n".format(kk, " ".join(result[0][kk])))
            nchain_pt=nchain_pt+nwalkers
            fchain.close()
            if(args.storetime==1):
               with open(file_time,'a') as ftime:
                  ftime.write('%f %f\n'%(mcutil.timediff(time_beg,time.localtime()),get_memoryuse(py)))
            #input('aa:')
            if(fact*(ss+1)>= wnow):
               wnow=wnow+width
               print("{0:5.1%}".format(float(ss) / nsteps),mcutil.ltime(date=True),nchain_pt)

      print("Finished MCMC saved chains in: %s  %s"%(chainfile,mcutil.ltime(date=True)))


      #print("Mean acceptance fraction: {0:.3f}"
      #                .format(np.mean(sampler.acceptance_fraction)))
   elif(cfg['bfit']==1):
      #if only ran with bfit =1 then redefine chainfile
      chainfile=cfg['outroot']+'.txt'


   #This runs after mcmc or for bfit!=0
   #write the stats
   bfitpars,mfitpars=mcutil.chainsTostats(cfg,chainfile)

   #write the bestfit HOD catalog and the correlation function
   #cfg=mcutil.BestFit(cfg,bfitpars,tag='bestfit')
   #cfg=mcutil.BestFit(cfg,mfitpars,tag='meanfit')

   if(use_mpi==1):
      # Close the processes.
      pool.close()

   #####################################################
   #Now make some plots
   #import corner
   #outfile=SHOD['outroot']+'_corner.png'
   #fig = corner.corner(samples, labels=SHOD['parlist'])
                               #truths=[m_true, b_true, np.log(f_true)])
   #fig.savefig(outfile)
   #print('saved: ',outfile)

   #####################################################
   #####################################################
