#!/usr/bin/env python

"""
This parse the config file and merge the command line input with config file.
It uses configobj library to parse the input file.
Follow the configobj library convention while creating the input file
"""

from __future__ import print_function,division
import numpy as np
import sys


import argparse

__author__ = "Shadab Alam"
__version__ = "1.0"
__email__  = "salam@roe.ac.uk"



def load_validate_config(config_file):
    '''loads the config file and validate input using configspec'''
    #it is imported here to require only if config_file is used
    import configobj, validate

    cfg=''' '''
    #validate
    spec = cfg.split("\n")
    config = configobj.ConfigObj(config_file, configspec=spec,interpolation=False)
    validator = validate.Validator()
    config.validate(validator, copy=True)
    config.filename = 'test'
    config.write()

    if(False):
        print('***********config*****')
        for tk in config.keys():
            print(tk, type(config[tk]), config[tk])
            if(type([])==type(config[tk])):
                for tv in config[tk]:
                    print('    ',type(tv),tv)

    return config


def merge_config(args):
    '''Reads the config file if given and merges/updates args using config file'''

    if(args.config_file==None):
        return args


    config=load_validate_config(args.config_file)
    #config = ConfigObj(args.config_file)

    #current key list
    args_list=args.__dict__.keys()

    #loop over config entry and transfer to the args
    for tt,tkey in enumerate(config.keys()):
        args.__dict__[tkey]=config[tkey]



    #if conditional string interpolation is needed then perform string interpolations
    if(args.Cinterpolation):
        #This package does conditional interpolation
        import conditional_string_interpolation as csi
        args.__dict__=csi.interp_dic(args.__dict__,verbose=args.interactive)

    return args


def type_cast_config():
    if isinstance(args.plots, int):
       print('gg')

def display_args(args,tab='   ',outfile=None):

    key_list=args.keys()
    if(outfile!=None):
        with open(outfile,'w') as fout:
            fout.write('Input values:\n')
            for tt,tkey in enumerate(key_list):
                fout.write('   ',tkey,' ',args[tkey])
    else:
        for tt,tkey in enumerate(key_list):
            print(tab,tkey,' ',args[tkey],type(args[tkey]))
            if(type(args[tkey])==dict):
                display_args(args[tkey],tab=tab+'   ',outfile=outfile)


    return


if __name__=="__main__":  
    parser = argparse.ArgumentParser(description='Parse the config file using configobj:')
    #these should be input
    parser.add_argument('-config_file',default=None,help='Input can be provided using a config file or command line. A mix of both  (i.e. command line and config file) can also be used. Some inputs can only be provided in config file. If an option is provided in config file and command line then the value in config file will be used.')

    args = parser.parse_args()
    print('args before:')

    import yaml
    with open(args.config_file) as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
        fruits_list = yaml.load(file, Loader=yaml.FullLoader)

    print(fruits_list)
    display_args(fruits_list)


    args=merge_config(args)
    
    print('args after:')
    diplay_args(args)


